"""{{cookiecutter.package_name}}"""
import logging

from pkg_resources import get_distribution, DistributionNotFound

from {{ cookiecutter.package_name }}.core import *
{%- if cookiecutter.command_line_interface != "no" %}
from {{cookiecutter.package_name}}.log import userinfo, verbose
{%- endif %}


{%- if cookiecutter.setup_py_uses_setuptools_scm != "no" %}
try:
    __version__ = get_distribution("{{cookiecutter.package_name}}").version
except DistributionNotFound:
    __version__ = "Please install package via setup.py"
{%- else %}
__version__ = "{{cookiecutter.version}}"
{%- endif %}


logger = logging.getLogger("{{cookiecutter.package_name}}")
logger.setLevel(logging.INFO)
{%- if cookiecutter.command_line_interface != "no" %}
logger.addHandler(userinfo)
logger.addHandler(verbose)
{%- else %}
logger.addHandler(logging.NullHandler())
{%- endif %}

