"""{{ cookiecutter.package_name }} cli"""
import sys

import {{ cookiecutter.package_name }}
{%- if cookiecutter.command_line_interface == "argparse" %}
from {{ cookiecutter.package_name }}.cli.parsers import parser
from {{ cookiecutter.package_name }}.cli.utils import CmdAction
{%- endif %}

def main(args=sys.argv[1:]):
    if "--debug" in args:
        {{ cookiecutter.package_name }}.logger.setLevel({{ cookiecutter.package_name }}.logging.DEBUG)
{%- if cookiecutter.command_line_interface == "argparse" %}
    if len(args) == 0 and "--debug" not in args:
        parser.print_help()

    parsed = parser.parse_args(args)

    actions = CmdAction(parsed)

    # Example action
    actions.add_multiple(
        [
            ("foobar", lambda foo, bar, baz: print(foo, bar, baz), ["foo_arg", "bar_arg", "baz_arg"]),
        ]
    )

    if parsed.cmd:
        actions.do(parsed.cmd)
{%- else %}
    print(f"arguments => {', '.join(args)}")
{%- endif %}

if __name__ == "__main__":
    main()
