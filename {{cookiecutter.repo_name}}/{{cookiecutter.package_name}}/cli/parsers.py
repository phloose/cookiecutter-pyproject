"""{{cookiecutter.package_name}} cli parsers setup"""
import argparse

from {{ cookiecutter.package_name }} import __version__

common_args = argparse.ArgumentParser(add_help=False)
common_args.add_argument(
    "--debug", action="store_true", default=False, help="Toggle debug output"
)

parser = argparse.ArgumentParser(
    prog="{{ cookiecutter.command_line_interface_bin_name }}", description="tba", parents=[common_args],
)
parser.add_argument(
    "--version", action="version", version=f"{parser.prog} {__version__}"
)

# Example subparser: "{{ cookiecutter.command_line_interface_bin_name }} foobar -f 123 -b 456 -z"

# "dest" will be the the reference in the Namespace object
subparser = parser.add_subparsers(dest="cmd")

sub_desc = """tba"""
sub = subparser.add_parser("foobar", parents=[common_args], help=sub_desc)
sub.add_argument("--foo", "-f", dest="foo_arg", required=True)
sub.add_argument("--bar", "-b", dest="bar_arg", required=False)
sub.add_argument("--baz", "-z", dest="baz_arg", action="store_true", default=False)
