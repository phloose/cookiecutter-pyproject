"""{{cookiecutter.package_name}} cli utilities"""
import logging

logger = logging.getLogger("{{ cookiecutter.package_name }}.cli.utils")


class CmdAction:
    """Register a function for an argparse commandline command

    Needs an argparse.Namespace object as input.
    """

    def __init__(self, parsed):
        self.parsed = parsed
        self.actions = {}

    def add(self, cmd: str, action, args: list):
        """Add an action and a list of arguments for an argparse command

        Args:
            cmd (str): The commands name. Should match with the corresponding command
                from the argparse configuration
            action (function): The action that should be performed. This is a reference
                to a function or method
            args (list): A list of arguments for the registered action

        Raises:
            ValueError: If action is not callable a ValueError is raised
        """
        if not callable(action):
            raise ValueError("Argument 'action' needs to be callable!")
        self.actions.update(
            {cmd: {"action": action, "args": {arg: None for arg in args}}}
        )

    def add_multiple(self, actions_list: list):
        """Register multiple actions at once

        Args:
            actions_list (list): A list containing tuples in the form
            (cmd, action, [arg1, arg2, ...])
        """
        for action in actions_list:
            self.add(*action)

    def do(self, cmd: str):
        """Execute a registered action via its command

        Args:
            cmd (str): The registered command

        Raises:
            KeyError: When trying to execute an action that is not registered a KeyError
             is raised

        Returns:
            The return value of the call to the registered action
        """
        cmd_name = cmd
        cmd = self.actions.get(cmd)
        if cmd is None:
            raise KeyError(f"{cmd_name!r} is not registered as an action")
        action = cmd.get("action")
        args = self._get_parsed_arg_values(cmd.get("args"))
        return action(**args)

    def _get_parsed_arg_values(self, cmd_args: dict):
        """Extracts the argument values from the parsed argparse.Namespace object

        Iterates through argparse.Namespace arguments and tries to match these against
        the command args that belong to a registered action.

        Args:
            cmd_args (dict): The args dictionary from a command of the actions
                dictionary

        Returns:
            (dict): The registered command args with values from matching
                argparse.Namespace arguments
        """
        for opt, val in vars(self.parsed).items():
            if opt in cmd_args:
                cmd_args[opt] = val
        return cmd_args
