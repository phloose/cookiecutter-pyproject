"""{{ cookiecutter.package_name }} core implementation"""
import logging

logger = logging.getLogger("{{ cookiecutter.package_name }}.core")


def corefunc(inp=None):
    """Core Func does stuff

    Args:
        inp (str, optional): A string. Defaults to None.

    Raises:
        Exception: Raise Exception when inp is None

    Returns:
        bool: Just return False
    """
    print("hello")

    if inp is None:
        raise Exception("Popel")

    return False


class Foo:
    """Foo is great

    Main container for fooing bars that bazz
    """

    def __init__(self, var1):
        self.var1 = var1

    def bar(self, value):
        """Bar doing fuzz

        Args:
            value (str): Something of value

        Returns:
            bool: Whether we have a value or not
        """
        if not value:
            return False
        return True

    def baz(self, othervalue):
        """Bazzing shizzle

        Args:
            othervalue (str): Something else

        Raises:
            ValueError: If it is then we crash

        Returns:
            None: None
        """
        if othervalue:
            raise ValueError
        return None
