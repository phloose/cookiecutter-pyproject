from setuptools import setup, find_packages

with open("README.md") as readme_file:
    long_description = readme_file.read()

with open("CHANGELOG.md") as changelog_file:
    changes = changelog_file.read()

setup(
    name="{{ cookiecutter.package_name }}",
    author={{ '{0!r}'.format(cookiecutter.full_name).lstrip('ub') }},
    author_email={{ '{0!r}'.format(cookiecutter.email).lstrip('ub') }},
{%- if cookiecutter.repo_hosting_domain == "no" %}
    url='file://' + os.path.abspath(dirname(__file__)),
{%- else %}
    url='https://{{ cookiecutter.repo_hosting_domain }}/{{ cookiecutter.repo_username }}/{{ cookiecutter.repo_name }}',
{%- endif %}
    description={{ '{0!r}'.format(cookiecutter.project_short_description).lstrip('ub') }},
    long_description=long_description + "\n\n" + changes,
    keywords={{ '{0!r}'.format(cookiecutter.project_keywords).lstrip('ub') }},
{%- if cookiecutter.license != "no" %}
    license='{{ {
        "BSD 2-Clause License": "BSD-2-Clause",
        "BSD 3-Clause License": "BSD-3-Clause",
        "MIT license": "MIT",
        "ISC license": "ISC",
        "Apache Software License 2.0": "Apache-2.0"}[cookiecutter.license]
    }}',
{%- endif %}
    packages=find_packages(exclude=["tests", "docs"]),
{%- if cookiecutter.command_line_interface != 'no' %}
    entry_points={
        'console_scripts': [
            '{{ cookiecutter.command_line_interface_bin_name }} = {{ cookiecutter.package_name }}.cli:main',
        ]
    },
{%- endif %}
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
{%- if cookiecutter.license == "no" %}
{%- elif cookiecutter.license in ["BSD 2-Clause License", "BSD 3-Clause License"] %}
        'License :: OSI Approved :: BSD License',
{%- elif cookiecutter.license == "MIT license" %}
        'License :: OSI Approved :: MIT License',
{%- elif cookiecutter.license == "ISC license" %}
        'License :: OSI Approved :: ISC License (ISCL)',
{%- elif cookiecutter.license == "Apache Software License 2.0" %}
        'License :: OSI Approved :: Apache Software License',
{%- endif %}
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: Unix",
        "Operating System :: POSIX",
        "Operating System :: Microsoft :: Windows",
    ],
    python_requires=">=3.6",
    extras_require={
        "dev": [
{%- if cookiecutter.test_framework == "pytest" %}
            "pytest>=5.1.0",
            "pytest-cov>=2.7.1",
            "pytest-mock>=1.10.4",
{%- endif %}
{%- if cookiecutter.linter == "flake8" %}
            "flake8>=3.7.8",
{%- endif %}
{%- if cookiecutter.formatter == "black" %}
            "black>=19.3b0",
{%- elif cookiecutter.formatter == "yapf" %}
            "yapf>=0.29.0",
{%- elif cookiecutter.formatter == "autopep8" %}
            "autopep8>=1.4.4",
{%- endif %}
{%- if cookiecutter.sphinx_docs != "no" %}
            "sphinx>=1.3",
            "{{ cookiecutter.sphinx_theme|replace('_', '-') }}"
{%- endif %}
        ]
    },
{%- if cookiecutter.setup_py_uses_setuptools_scm != "no" %}
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
{%- endif %}
    project_urls={
        "Changelog": "https://{{ cookiecutter.repo_hosting_domain }}/{{ cookiecutter.repo_username }}/{{ cookiecutter.repo_name }}/blob/master/CHANGELOG.md",
        "Bug Tracker": "https://{{ cookiecutter.repo_hosting_domain }}/{{ cookiecutter.repo_username }}/{{ cookiecutter.repo_name }}/issues"
        },
)
