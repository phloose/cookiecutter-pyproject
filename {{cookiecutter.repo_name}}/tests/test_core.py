""" Unit tests for the {{ cookiecutter.package_name }}.core module """
{%- if cookiecutter.test_framework == "pytest" %}
import pytest
{%- else  %}
import unittest
{%- endif %}

import {{ cookiecutter.package_name }}.core
