@ECHO OFF

pushd %~dp0

REM Command file for Sphinx documentation

if "%SPHINXBUILD%" == "" (
	set SPHINXBUILD=sphinx-build
)

if "%SPHINXAUTOGEN%" == "" (
    set SPHINXAUTOGEN=sphinx-autogen
)

if "%SPHINXAPIDOC%" == "" (
    set SPHINXAPIDOC=sphinx-apidoc
)

set SOURCEDIR=.
set BUILDDIR=_build

if "%1" == "" goto all

if "%1" == "all" goto all

if "%1" == "help" goto help

if "%1" == "html" goto html

if "%1" == "clean" goto clean

if "%1" == "api" goto api

REM if "%1" == "moduleindex" goto moduleindex

%SPHINXBUILD% >NUL 2>NUL
if errorlevel 9009 (
	echo.
	echo.The 'sphinx-build' command was not found. Make sure you have Sphinx
	echo.installed, then set the SPHINXBUILD environment variable to point
	echo.to the full path of the 'sphinx-build' executable. Alternatively you
	echo.may add the Sphinx directory to PATH.
	echo.
	echo.If you don't have Sphinx installed, grab it from
	echo.http://sphinx-doc.org/
	exit /b 1
)

:all
REM %SPHINXAPIDOC% -e -f -M -o modules\ ..\{{ cookiecutter.package_name }}
%SPHINXAUTOGEN% -i -t _templates\ -o api\generated\ api\index.rst
%SPHINXBUILD% -M html %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%
goto end

:clean
rmdir _build\ /s /q
del /s /q api\generated\*.rst
del /s /q modules\*.rst
goto end

:html
%SPHINXBUILD% -M %1 %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%
goto end

:api
%SPHINXAUTOGEN% -i -o api\generated\ api\index.rst
goto end

REM :moduleindex
REM %SPHINXAPIDOC% -e -f -M -o modules\ ..\{{ cookiecutter.package_name }}
REM goto end

:help
%SPHINXBUILD% -M help %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %O%

:end
popd
