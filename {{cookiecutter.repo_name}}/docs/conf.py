# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

source_suffix = '.rst'
master_doc = 'index'
project = {{ '{0!r}'.format(cookiecutter.project_name) }}
year = '{% if cookiecutter.year_from == cookiecutter.year_to %}{{ cookiecutter.year_from }}{% else %}{{ cookiecutter.year_from }}-{{ cookiecutter.year_to }}{% endif %}'
author = {{ '{0!r}'.format(cookiecutter.full_name) }}
copyright = '{0}, {1}'.format(year, author)

# The full version, including alpha/beta/rc tags
{%- if cookiecutter.setup_py_uses_setuptools_scm == 'yes' %}
try:
    from pkg_resources import get_distribution
    version = release = get_distribution('{{ cookiecutter.package_name }}').version
except Exception:
    import traceback

    traceback.print_exc()
    version = release = {{ '{0!r}'.format(cookiecutter.version) }}
{%- else %}
version = release = {{ '{0!r}'.format(cookiecutter.version) }}
{%- endif %}


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.coverage",
    "sphinx.ext.autosummary",
    "sphinx.ext.viewcode",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

extlinks = {
    'issue': ('https://{{ cookiecutter.repo_hosting_domain }}/{{ cookiecutter.repo_username }}/{{ cookiecutter.repo_name }}/issues/%s', '#'),
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# -- Options for HTML output -------------------------------------------------

{%- if cookiecutter.sphinx_theme != 'sphinx-rtd-theme' %}

html_theme = "{{ cookiecutter.sphinx_theme|replace('-', '_') }}"

{%- else %}
# on_rtd is whether we are on readthedocs.org
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

if not on_rtd:  # only set the theme if we're building docs locally
    html_theme = 'sphinx_rtd_theme'
{%- endif %}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

html_use_smartypants = True
html_last_updated_fmt = '%b %d, %Y'
html_split_index = False
html_show_sourcelink = False

# html_sidebars = {
#    '**': ['searchbox.html', 'globaltoc.html', 'sourcelink.html'],
# }
html_short_title = '%s-%s' % (project, version)

napoleon_use_ivar = True
napoleon_use_rtype = False
napoleon_use_param = False


# -- Extension configuration -------------------------------------------------

autosummary_generated = True

autodoc_default_options = {
    "member-order": "bysource",
    "undoc-members": False,
}
