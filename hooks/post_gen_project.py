from __future__ import print_function

import datetime
import os
import shutil
import subprocess
import sys
from os.path import join


def unlink_if_exists(path):
    if os.path.exists(path):
        os.unlink(path)

if __name__ == "__main__":

{% if cookiecutter.sphinx_docs == "no" %}
    shutil.rmtree('docs')
    os.unlink('.readthedocs.yml')
{%- else %}
{%- if 'readthedocs' not in cookiecutter.sphinx_docs_hosting %}
    os.unlink('.readthedocs.yml')
{% endif %}
{% endif %}

{%- if cookiecutter.command_line_interface == 'no' %}
    os.unlink(join("{{ cookiecutter.package_name }}", "__main__.py"))
    os.unlink(join("{{ cookiecutter.package_name }}", "log.py"))
    shutil.rmtree(join("{{ cookiecutter.package_name}}", "cli"))
    shutil.rmtree(join("tests", "cli"))
{%- elif cookiecutter.command_line_interface == "plain" %}
    os.unlink(join("{{ cookiecutter.package_name }}", "cli", "parsers.py"))
    os.unlink(join("{{ cookiecutter.package_name }}", "cli", "utils.py"))
{% endif %}

{%- if cookiecutter.use_vscode == "no" %}
    shutil.rmtree(".vscode")
{%- endif %}

{%- if cookiecutter.use_conda_env == "no" %}
    os.unlink("env.yml")
{%- endif %}

{%- if cookiecutter.formatter != "black" %}
    os.unlink("pyproject.toml")
{%- endif %}

{%- if cookiecutter.license == "no" %}
    os.unlink('LICENSE')
{% endif %}

{%- if cookiecutter.test_framework != "pytest" %}
    os.unlink(join("tests", "conftest.py"))
{%- endif %}

    print("""
################################################################################
################################################################################

    You have succesfully created `{{ cookiecutter.repo_name }}`.

################################################################################

    You've used these cookiecutter parameters:
{% for key, value in cookiecutter.items()|sort %}
        {{ "{0:26}".format(key + ":") }} {{ "{0!r}".format(value).strip("u") }}
{%- endfor %}

    See .cookiecutterrc for instructions on regenerating the project.

################################################################################

    To get started run these:

        cd {{ cookiecutter.repo_name }}
        git init
        git add --all
        git commit -m "Initial commit"
        git remote add origin git@{{ cookiecutter.repo_hosting_domain }}:{{ cookiecutter.repo_username }}/{{ cookiecutter.repo_name }}.git
        git push -u origin master v{{ cookiecutter.version }}

""")

    command_line_interface_bin_name = '{{ cookiecutter.command_line_interface_bin_name }}'
    while command_line_interface_bin_name.endswith('.py'):
        command_line_interface_bin_name = command_line_interface_bin_name[:-3]

        if command_line_interface_bin_name == '{{ cookiecutter.package_name }}':
            warn("""
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                                            !!
!!      ERROR:                                                                !!
!!                                                                            !!
!!          Your result package is broken. Your bin script named              !!
!!          {0} !!
!!                                                                            !!
!!          Python automatically adds the location of scripts to              !!
!!          `sys.path`. Because of that, the bin script will fail             !!
!!          to import your package because it has the same name               !!
!!          (it will try to import itself as a module).                       !!
!!                                                                            !!
!!          To avoid this problem you have two options:                       !!
!!                                                                            !!
!!          * Remove the ".py" suffix from the `command_line_interface_bin_name`.                    !!
!!                                                                            !!
!!          * Use a different `package_name` {1} !!
!!                                                                            !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
""".format(
                '"{{ cookiecutter.command_line_interface_bin_name }}" will shadow your package.'.ljust(65),
                '(not "{0}").'.format(command_line_interface_bin_name).ljust(32)))
            sys.exit(1)
        break
