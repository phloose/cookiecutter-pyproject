# pyproject template

This is my personal pyproject template.

Inspired a lot by [ionelmc/cookiecutter-pylibrary](https://github.com/ionelmc/cookiecutter-pylibrary) and for the sphinx configurations by [GenericMappingTools/pygmt](https://github.com/GenericMappingTools/pygmt).
